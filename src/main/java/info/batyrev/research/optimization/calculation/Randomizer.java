package info.batyrev.research.optimization.calculation;

import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.utils.Constants;

import java.util.Date;
import java.util.Random;

public class Randomizer {

    private static Random random = new Random((new Date()).getTime());

    public static Point getPoint() {
        Point point = new Point();

        for (int i = 0; i < point.size(); i++) {
            double sign = 1.0;//random.nextBoolean() ? 1.0 : -1.0;
            double value = sign * random.nextDouble() * Constants.Global.DELTA;
            point.set(i, value);
        }

        return point;
    }
}
