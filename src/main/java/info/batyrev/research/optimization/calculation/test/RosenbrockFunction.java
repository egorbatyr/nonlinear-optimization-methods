package info.batyrev.research.optimization.calculation.test;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.utils.MathUtils;

public class RosenbrockFunction implements Calculable {

    @Override
    public double calculate(Point point) {
        double result = 0.0;

        for (int i = 0; i < point.size() - 1; i++) {
            double value = 100.0 * MathUtils.sqr(point.get(i + 1) - MathUtils.sqr(point.get(i)))
                    + MathUtils.sqr(1.0 - point.get(i));
            result += value;
        }

        return result;
    }
}
