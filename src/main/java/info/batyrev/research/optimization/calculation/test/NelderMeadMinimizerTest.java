package info.batyrev.research.optimization.calculation.test;

import info.batyrev.research.optimization.calculation.Randomizer;
import info.batyrev.research.optimization.core.Minimizer;
import info.batyrev.research.optimization.core.NelderMeadMinimizer;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.core.model.ResultPair;
import info.batyrev.research.optimization.core.model.Simplex;
import info.batyrev.research.optimization.core.model.Space;
import info.batyrev.research.optimization.utils.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class NelderMeadMinimizerTest {

    private static final int COUNT_DIMENSIONS = 2;
    private static final int NUM_AREAS = 100;
    private static final double EPS = 1e-4;

    private List<Simplex> simplexList;

    @Before
    public void before() {
        Space.setDimension(COUNT_DIMENSIONS);
        simplexList = new ArrayList<>();

        for (int i = 0; i < NUM_AREAS; i++) {
            Simplex simplex = new Simplex();

            for (int j = 0; j < simplex.size(); j++) {
                Point point = Randomizer.getPoint();
                simplex.setVertex(j, point);
            }

            simplexList.add(simplex);
        }
    }

    @Test
    public void himmelblauFunctionTest() {
        System.err.println("info.batyrev.research.optimization.calculation.test.NelderMeadMinimizerTest.himmelblauFunctionTest");
        final double[] ans = {Double.MAX_VALUE};
        final Point[] point = {new Point()};

        simplexList.parallelStream().forEach(simplex -> {
                    Minimizer minimizer = new NelderMeadMinimizer(new HimmelblauFunction(), simplex, EPS);
                    ResultPair resultPair = minimizer.run();

                    if (resultPair.getFunctionValue() + Constants.Global.EPS < ans[0]) {
                        ans[0] = resultPair.getFunctionValue();
                        point[0] = resultPair.getPoint();
                    }
                }
        );

        System.err.printf("%.5f ", ans[0]);
        System.err.println(point[0].toString());

        boolean assertValue = ans[0] < EPS;
        Assert.assertTrue(assertValue);
    }

    @Test
    public void ackleyFunctionTest() {
        System.err.println("info.batyrev.research.optimization.calculation.test.NelderMeadMinimizerTest.ackleyFunctionTest");
        final double[] ans = {Double.MAX_VALUE};
        final Point[] point = {new Point()};

        simplexList.parallelStream().forEach(simplex -> {
                    Minimizer minimizer = new NelderMeadMinimizer(new AckleyFunction(), simplex, EPS);
                    ResultPair resultPair = minimizer.run();

                    if (resultPair.getFunctionValue() + Constants.Global.EPS < ans[0]) {
                        ans[0] = resultPair.getFunctionValue();
                        point[0] = resultPair.getPoint();
                    }
                }
        );

        System.err.printf("%.5f ", ans[0]);
        System.err.println(point[0].toString());

        boolean assertValue = ans[0] < EPS;
        Assert.assertTrue(assertValue);
    }
}
