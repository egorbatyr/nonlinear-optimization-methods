package info.batyrev.research.optimization.calculation;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.core.model.Space;
import info.batyrev.research.optimization.utils.Constants;
import info.batyrev.research.optimization.utils.MathUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondOptimizationFunction implements Calculable {

    private double length;
    private List<Double> time;
    private double psy;
    private double m;
    private double n;
    private List<Double> tetaq;

    public SecondOptimizationFunction(
            double length,
            List<Double> time,
            double psy,
            double m,
            double n,
            List<Double> tetaq
    ) {
        this.length = length;
        this.time = time;
        this.psy = psy;
        this.m = m;
        this.n = n;
        this.tetaq = tetaq;
    }

    private double phi(double tetaj, double psy, double m, double n, Point p) {
        double temp1 = (p.get(0) * psy * m) / (n * tetaj * tetaj);
        double temp2 = Math.pow((m / tetaj) - 1.0, (1.0 / n) - 1.0);
        double top = temp1 * temp2;

        double temp3 = Math.pow(psy / p.get(2), p.get(1));
        double temp4 = Math.pow(m / tetaj - 1.0, p.get(1) / n);
        double down = 1.0 + temp3 * temp4;

        return top / down;
    }

    private double temp(Point point, double timeS, double tetaj) {
        double sum = 0.0;

        for (int i = 1; i < 100; i++) {
            double temp1 = 8.0 / (Math.PI * Math.PI * MathUtils.sqr(2.0 * i - 1.0));
            double temp2 = - phi(tetaj, psy, m, n, point) / MathUtils.sqr(length);
            double temp3 = Math.PI * Math.PI * MathUtils.sqr(2.0 * i - 1.0) * timeS / 4.0;
            double res = temp1 * Math.exp(temp2 * temp3);
            sum += res;
        }

        return sum;
    }

    @Override
    public double calculate(Point point) {
        double result = 0.0;

        double tetaj = 0;

        for (int i = 0; i < tetaq.size(); i++) {
            if (i < 9) {
                tetaj = 0.335724638;
            } else if (i < 17) {
                tetaj = 0.328176329;
            } else if (i < 29) {
                tetaj = 0.312173913;
            } else if (i < 48) {
                tetaj = 0.2952657;
            } else if (i < 54) {
                tetaj = 0.291944444;
            } else if (i < 65) {
                tetaj = 0.285;
            }
//            if (i < 5) {
//                tetaj = 0.403554945;
//            } else if (i < 9) {
//                tetaj = 0.377456044;
//            } else if (i < 16) {
//                tetaj = 0.347111389;
//            } else if (i < 25) {
//                tetaj = 0.32887962;
//            } else if (i < 32) {
//                tetaj = 0.316017483;
//            }

            double tetaqi = tetaq.get(i);
            double timei = time.get(i);
            double series = temp(point, time.get(i), tetaj);
            series = series * (tetaj - tetaqi) + tetaj;
            double subSum = MathUtils.sqr(series - tetaq.get(i));
            result += subSum;
        }

        return result;
    }

    public static void main(String[] args) {
        Space.setDimension(3);
        Point point = new Point(
                Arrays.asList(1.5515739518765348, 0.9704305033025586, 0.748398485506792)
        );

        double length = Constants.SecondOptimization.L;
        double psy = Constants.SecondOptimization.psy_S;
        double m = Constants.SecondOptimization.m;
        double n = Constants.SecondOptimization.n;

        List<Double> time = new ArrayList<>();
        List<Double> gtime = new ArrayList<>();
        double localTime = 0, globalTime = 0.0;

        for (int i = 0; i < Constants.InputData.timeSet2.size(); i++) {
            if (i == 9 || i == 17 || i == 29 || i == 48
                    || i == 54) {
                localTime = 0.0;
            } else {
                localTime += Constants.InputData.timeSet2.get(i);
            }

            globalTime += Constants.InputData.timeSet2.get(i);
            gtime.add(globalTime);

            time.add(localTime);
        }

        SecondOptimizationFunction function = new SecondOptimizationFunction(
                length, time, psy, m, n, Constants.InputData.tetaqSet2
        );

        double result = 0.0;
        double tetaj = 0;

        for (int i = 0; i < function.tetaq.size(); i++) {
            if (i < 9) {
                tetaj = 0.335724638;
            } else if (i < 17) {
                tetaj = 0.328176329;
            } else if (i < 29) {
                tetaj = 0.312173913;
            } else if (i < 48) {
                tetaj = 0.2952657;
            } else if (i < 54) {
                tetaj = 0.291944444;
            } else if (i < 65) {
                tetaj = 0.285;
            }

//            if (i < 5) {
//                tetaj = 0.403554945;
//            } else if (i < 9) {
//                tetaj = 0.377456044;
//            } else if (i < 16) {
//                tetaj = 0.347111389;
//            } else if (i < 25) {
//                tetaj = 0.32887962;
//            } else if (i < 32) {
//                tetaj = 0.316017483;
//            }

            double tetaqi = function.tetaq.get(i);
            double timei = time.get(i);
            double series = function.temp(point, time.get(i), tetaj);
            series = series * (tetaj - tetaqi) + tetaj;
            double subSum = Math.abs(series - function.tetaq.get(i));

            System.err.printf("Calc_value = %.5f\tExperimental_value = %.5f\tdelta = %.5f\n",
                    series, function.tetaq.get(i), Math.abs(subSum));

            result += MathUtils.sqr(subSum);
        }

        System.err.printf("%.10f", result);
    }
}
