package info.batyrev.research.optimization.calculation.test;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.utils.MathUtils;

public class HimmelblauFunction implements Calculable {

    @Override
    public double calculate(Point point) {
        return MathUtils.sqr(MathUtils.sqr(point.get(0)) + point.get(1) - 11.0)
                + MathUtils.sqr(point.get(0) + MathUtils.sqr(point.get(1)) - 7.0);
    }
}
