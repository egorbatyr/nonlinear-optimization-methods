package info.batyrev.research.optimization.calculation;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.core.model.Space;
import info.batyrev.research.optimization.utils.Constants;
import info.batyrev.research.optimization.utils.InterpolationWorks;
import info.batyrev.research.optimization.utils.MathUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FirstOptimizationFunction implements Calculable {

    private List<Double> psy;
    private List<Double> teta;

    public FirstOptimizationFunction(List<Double> psy, List<Double> teta) {
        this.psy = psy;
        this.teta = teta;
    }

    @Override
    public double calculate(Point point) {
        double result = 0;

        for (int i = 0; i < psy.size(); i++) {
            double value = point.get(0) / (1.0 + Math.pow(psy.get(i) / point.get(2), point.get(1)));
            double subSum = MathUtils.sqr(point.get(0) /
                    (1.0 + Math.pow(psy.get(i) / point.get(2), point.get(1))) - teta.get(i));
            result += subSum;
        }

        return result;
    }

    private static void check1() {
        Space.setDimension(3);
        Point point = new Point(
                Arrays.asList(0.4903803276013837, 0.45566328191712446, 14.391313503516455)
        );

        FirstOptimizationFunction function = new FirstOptimizationFunction(
                Constants.FirstOptimization.psy,
                Constants.FirstOptimization.teta
        );

        int n = Constants.FirstOptimization.psy.size();

        for (int i = 0; i < n; i++) {
            double value = point.get(0) / (1.0 + Math.pow(function.psy.get(i) / point.get(2), point.get(1)));
            double subSum = point.get(0) / (1.0 + Math.pow(function.psy.get(i) / point.get(2), point.get(1))) - function.teta.get(i);
            System.err.printf("Calc_value = %.5f\tExperimental_value = %.5f\tdelta = %.5f\n",
                    value, function.teta.get(i), Math.abs(subSum));
        }

        double fRes = function.calculate(point);
        System.err.printf("omega = %.10f", fRes);
    }

    private static void check2() {
        Space.setDimension(3);
        Point point = new Point(
                Arrays.asList(0.3660989609607117, 0.539803860205901, 49.83087923634787)
        );

        FirstOptimizationFunction function = new FirstOptimizationFunction(
                Constants.FirstOptimization.psy,
                Constants.FirstOptimization.teta
        );

        int n = Constants.FirstOptimization.psy.size();

        for (int i = 0; i < n; i++) {
            double value = point.get(0) / (1.0 + Math.pow(function.psy.get(i) / point.get(2), point.get(1)));
            double subSum = point.get(0) / (1.0 + Math.pow(function.psy.get(i) / point.get(2), point.get(1))) - function.teta.get(i);
            System.err.printf("Calc_value = %.5f\tExperimental_value = %.5f\tdelta = %.5f\n",
                    value, function.teta.get(i), Math.abs(subSum));
        }

        double fRes = function.calculate(point);
        System.err.printf("omega = %.10f", fRes);
    }

    public static void main(String[] args) {
        check2();
    }
}
