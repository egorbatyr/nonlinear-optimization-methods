package info.batyrev.research.optimization.calculation.test;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.utils.MathUtils;

public class AckleyFunction implements Calculable {

    @Override
    public double calculate(Point point) {
        double x = point.get(0);
        double y = point.get(1);

        double term_a = -20.0 * Math.exp(-0.2 * Math.sqrt(0.5 * (MathUtils.sqr(x) + MathUtils.sqr(y))));
        double term_b = - Math.exp(0.5 * (Math.cos(2.0 * Math.PI * x) + Math.cos(2.0 * Math.PI * y)));
        return term_a + term_b + Math.E + 20.0;
    }
}
