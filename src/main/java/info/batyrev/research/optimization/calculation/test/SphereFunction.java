package info.batyrev.research.optimization.calculation.test;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.utils.MathUtils;

public class SphereFunction implements Calculable {

    @Override
    public double calculate(Point point) {
        double result = 0.0;

        for (int i = 0; i < point.size(); i++) {
            result += MathUtils.sqr(point.get(i));
        }

        return result;
    }
}
