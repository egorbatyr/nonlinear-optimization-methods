package info.batyrev.research.optimization.calculation;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.core.model.Space;
import info.batyrev.research.optimization.utils.Constants;
import info.batyrev.research.optimization.utils.MathUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondOptimizationFunctionUpdate {

    private static double length = Constants.SecondOptimization.L;
    private static List<Double> timex;
    private static double psy = Constants.SecondOptimization.psy_S;
    private static double m = Constants.SecondOptimization.m;
    private static double n = Constants.SecondOptimization.n;
    private static List<Double> tetaq = Constants.InputData.tetaqSet1;

    static {
        List<Double> time = new ArrayList<>();
        List<Double> gtime = new ArrayList<>();
        double localTime = 0, globalTime = 0.0;

        for (int i = 0; i < Constants.InputData.timeSet1.size(); i++) {
            if (i == 6 || i == 10 || i == 17 || i == 26) {
                localTime = 0.0;
            } else {
                localTime += Constants.InputData.timeSet1.get(i);
            }

            globalTime += Constants.InputData.timeSet1.get(i);
            gtime.add(globalTime);

            time.add(localTime);
        }

        timex = time;
    }

    private static double phi(double tetaj, double psy, double m, double n, Point p) {
        double temp1 = (p.get(0) * psy * m) / (n * tetaj * tetaj);
        double temp2 = Math.pow((m / tetaj) - 1.0, (1.0 / n) - 1.0);
        double top = temp1 * temp2;

        double temp3 = Math.pow(psy / p.get(2), p.get(1));
        double temp4 = Math.pow(m / tetaj - 1.0, p.get(1) / n);
        double down = 1.0 + temp3 * temp4;

        return top / down;
    }

    private static double temp(Point point, double timeS, double tetaj) {
        double sum = 0.0;

        for (int i = 1; i < 100; i++) {
            double temp1 = 8.0 / (Math.PI * Math.PI * MathUtils.sqr(2.0 * i - 1.0));
            double temp2 = - phi(tetaj, psy, m, n, point) / MathUtils.sqr(length);
            double temp3 = Math.PI * Math.PI * MathUtils.sqr(2.0 * i - 1.0) * timeS / 4.0;
            double res = temp1 * Math.exp(temp2 * temp3);
            sum += res;
        }

        return sum;
    }

    public static double calculate(Point point) {
        double result = 0.0;

        double tetaj = 0;

        for (int i = 0; i < tetaq.size(); i++) {
            if (i < 5) {
                tetaj = 0.403537463;
            } else if (i < 9) {
                tetaj = 0.377456044;
            } else if (i < 16) {
                tetaj = 0.347111389;
            } else if (i < 25) {
                tetaj = 0.32887962;
            } else if (i < 32) {
                tetaj = 0.316017483;
            }

            double tetaqi = tetaq.get(i);
            double timei = timex.get(i);
            double series = temp(point, timex.get(i), tetaj);
            series = series * (tetaj - tetaqi) + tetaj;
            double subSum = MathUtils.sqr(series - tetaq.get(i));
            System.out.println("series = " + series + " subsum = " + subSum);
            result += subSum;
        }

        return result;
    }

    public static void main(String[] args) {
        Space.setDimension(3);
        Point point = new Point(Arrays.asList(0.072945, 1.821, 1.5));

        double temp = calculate(point);
        System.out.println(temp);
    }
}
