package info.batyrev.research.optimization.utils;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

public interface Constants {

    class Global {
        public static final double EPS = 1e-5;
        public static final double DELTA = 10.0;

        public static final int NUM_AREAS = 40;//1000;
        public static final int ITER_MAX = (int) 1e4;
        public static final int MAXN = 1;
        public static final int CNT_DIMENSIONS = 3;
    }

    class NelderMead {
        public static final double ALPHA = 1.0;
        public static final double BETA = 0.5;
        public static final double GAMMA = 2.0;
    }

    class System {
        public static final Writer STD_WRITER = new PrintWriter(java.lang.System.out);
    }

    class FirstOptimization {
        public static final List<Double> psy = Arrays.asList(
                //0.04, 0.54, 1.04, 2.04, 3.04, /*5.04*/ 4.04
                0.04, 0.54, 1.04, 2.04, 3.04, 4.04, 5.04
        );
        public static final List<Double> teta = Arrays.asList(
                //0.4585, 0.4034, 0.3746, 0.3465, 0.3277, 0.316
                0.436871981, 0.335422705, 0.328176329, 0.312173913, 0.2952657, 0.291944444, 0.285
        );
    }

    class FirstOptimizationInterpolated {
        public static final List<Double> psy = Arrays.asList(
                0.04, 0.27, 0.54, 0.79,
                1.04, 1.29, 1.54, 1.79,
                2.04, 2.29, 2.54, 2.79,
                3.04, 3.29, 3.54, 3.79,
                4.04, 4.29, 4.54, 4.79,
                5.04
        );
        public static final List<Double> teta = Arrays.asList(
                0.436871981, InterpolationWorks.interpolate(0.27), 0.335422705, InterpolationWorks.interpolate(0.79),
                0.328176329, InterpolationWorks.interpolate(1.29), InterpolationWorks.interpolate(1.54), InterpolationWorks.interpolate(1.79),
                0.312173913, InterpolationWorks.interpolate(2.29), InterpolationWorks.interpolate(2.54), InterpolationWorks.interpolate(2.79),
                0.2952657, InterpolationWorks.interpolate(3.29), InterpolationWorks.interpolate(3.54), InterpolationWorks.interpolate(3.79),
                0.291944444, InterpolationWorks.interpolate(4.29), InterpolationWorks.interpolate(4.54), InterpolationWorks.interpolate(4.79),
                0.285
        );
    }

    class SecondOptimization {
        public static final double L = 4;
//        public static final double psy_S =  /*17.153961942087527; //*/ 14.391313503516455;
//        public static final double m = /*0.5068054587158408; //*/ 0.4903803276013837;
//        public static final double n = /*0.3754858464644581; //*/ 0.45566328191712446;
        public static final double psy_S = 49.83087923634787;
        public static final double m = 0.3660989609607117;
        public static final double n = 0.539803860205901;
    }

    class InputData {

        public static final List<Double> timeSet1 = Arrays.asList(
                0.0, 23.0, 23.5, 23.7, 24.0,
                23.5, 23.5, 25.0, 24.4,
                22.6, 47.7, 24.4, 23.6,
                23.5, 48.0, 30.5, 65.5,
                24.5, 25.3, 22.2, 24.0,
                72.0, 48.0, 24.0, 72.7,
                23.5, 24.7, 23.0, 24.4,
                25.2, 23.8, 48.4
        );

        public static final List<Double> timeSet2 = Arrays.asList(
                0.0, 0.3, 1.0, 0.5,
                1.2, 3.8, 15.8, 2.4,
                15.2, 24.2, 12.3, 2.0,
                2.3, 2.8, 7.3, 19.1,
                2.2, 19.2, 6.8, 8.0,
                14.0, 8.4, 17.6, 6.3,
                17.4, 8.1, 25.2, 20.5,
                18.2, 8.3, 15.7, 7.9,
                17.2, 7.2, 15.8, 3.2,
                5.5, 15.2, 8.3, 16.0,
                7.5, 16.3, 7.9, 17.9,
                6.5, 15.8, 8.5, 16.0,
                8.3, 15.7, 8.4, 20.8,
                4.2, 16.1, 7.2, 17.4,
                7.2, 16.0, 7.0, 21.0,
                25.7, 20.3, 6.2, 16.8, 18.6
        );

        public static final List<Double> tetaqSet1 = Arrays.asList(
                0.458482517, 0.413527473,
                0.408657343, 0.404911089,
                0.403787213, 0.403537463,
                0.403412587, 0.384931069,
                0.377688312, 0.377438561,
                0.374566434, 0.356709291,
                0.352088911, 0.350465534,
                0.348467532, 0.347843157,
                0.347093906, 0.34646953,
                0.338602398, 0.336104895,
                0.334731269, 0.333607393,
                0.332608392, 0.331859141,
                0.329736264, 0.328862138,
                0.327738262, 0.324741259,
                0.320995005, 0.320120879,
                0.317498501, 0.316124875
        );

        public static final List<Double> tetaqSet2 = Arrays.asList(
                0.436871981, 0.427512077,
                0.388562802, 0.382826087,
                0.372560386, 0.356256039,
                0.35384058, 0.338743961,
                0.33602657, 0.335724638,
                0.335422705, 0.335120773,
                0.334818841, 0.332705314,
                0.332101449, 0.331195652,
                0.330289855, 0.329082126,
                0.328176329, 0.327572464,
                0.326062802, 0.320628019,
                0.31821256, 0.317608696,
                0.317004831, 0.316099034,
                0.315797101, 0.313985507,
                0.313381643, 0.312475845,
                0.312173913, 0.310966184,
                0.309456522, 0.308248792,
                0.30673913, 0.305531401,
                0.304021739, 0.30281401,
                0.301908213, 0.301002415,
                0.299794686, 0.298586957,
                0.297983092, 0.297681159,
                0.297077295, 0.29647343,
                0.296171498, 0.295869565,
                0.295567633, 0.2952657,
                0.294963768, 0.294359903,
                0.293756039, 0.293454106,
                0.293152174, 0.291944444,
                0.291642512, 0.290434783,
                0.29013285, 0.288925121,
                0.287415459, 0.286811594,
                0.286509662, 0.285905797,
                0.285603865
        );
    }
}
