package info.batyrev.research.optimization.utils;

import java.util.List;

public class InterpolationWorks {

    private static List<Double> xValues = Constants.FirstOptimization.psy;
    private static List<Double> yValues = Constants.FirstOptimization.teta;

    public static double interpolate(double x) {
        double result = 0.0;

        for (int i = 0; i< xValues.size(); i++) {
            double basicsPol = 1;

            for (int j = 0; j < xValues.size(); j++) {
                if (j != i) {
                    basicsPol *= (x - xValues.get(j))/(xValues.get(i) - xValues.get(j));
                }
            }

            result += basicsPol * yValues.get(i);
        }

        return result;
    }
}

// https://ru.wikibooks.org/wiki/Реализации_алгоритмов/Интерполяция/Многочлен_Лагранжа

