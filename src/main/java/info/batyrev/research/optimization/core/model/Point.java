package info.batyrev.research.optimization.core.model;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Point implements Printable {

    private static int COORDINATES_COUNT = Space.getDimension();

    private List<Double> x;

    public Point() {
        x = new ArrayList<>();
        for (int i = 0; i < COORDINATES_COUNT; i++) {
            x.add(0.0);
        }
    }

    public Point(List<Double> p) {
        if (p.size() != COORDINATES_COUNT) {
            throw new IllegalArgumentException("Incorrect number of coordinates");
        }

        x = new ArrayList<>();
        x.addAll(p);
    }

    public double get(int i) {
        return x.get(i);
    }

    public List<Double> getX() {
        return x;
    }

    public void set(int i, double val) {
        x.set(i, val);
    }

    public void setX(List<Double> x) {
        this.x = x;
    }

    public int size() {
        return x.size();
    }

    public Point add(Point p) {
        if (p.size() != x.size()) {
            throw new IllegalArgumentException("Unequal number of coordinates");
        }

        Point res = new Point();

        for (int i = 0; i < x.size(); i++) {
            double val = x.get(i) + p.get(i);
            res.set(i, val);
        }

        return res;
    }

    public Point sub(Point p) {
        if (p.size() != x.size()) {
            throw new IllegalArgumentException("Unequal number of coordinates");
        }

        Point res = new Point();

        for (int i = 0; i < x.size(); i++) {
            double val = x.get(i) - p.get(i);
            res.set(i, val);
        }

        return res;
    }

    public Point mult(double scalar) {
        Point res = new Point();

        for (int i = 0; i < x.size(); i++) {
            double val = x.get(i) * scalar;
            res.set(i, val);
        }

        return res;
    }

    public Point div(double scalar) {
        Point res = new Point();

        for (int i = 0; i < x.size(); i++) {
            double val = x.get(i) / scalar;
            res.set(i, val);
        }

        return res;
    }

    public double calcDistance(Point p) {
        if (p.size() != x.size()) {
            throw new IllegalArgumentException("Unequal number of coordinates");
        }

        double sum = 0.0;

        for (int i = 0; i < x.size(); i++) {
            sum += (x.get(i) - p.get(i)) * (x.get(i) - p.get(i));
        }

        return Math.sqrt(sum);
    }

    @Override
    public void print(Writer writer) {
        try {
            for (int i = 0; i < x.size(); i++) {
                String coordinate = "x [ " + i + " ] = " + x.get(i) + "\n";
                writer.write(coordinate);
            }
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Point{" + x.stream().map(Object::toString).collect(Collectors.joining(", ")) + "}";
    }
}
