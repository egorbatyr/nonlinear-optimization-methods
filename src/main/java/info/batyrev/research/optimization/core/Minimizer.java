package info.batyrev.research.optimization.core;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.ResultPair;

public abstract class Minimizer {

    protected Calculable function;
    protected double precission;

    public Minimizer(Calculable function, double precission) {
        this.function = function;
        this.precission = precission;
    }

    public abstract ResultPair run();

    protected abstract boolean terminationCondition();
}
