package info.batyrev.research.optimization.core;

import info.batyrev.research.optimization.core.model.Calculable;
import info.batyrev.research.optimization.core.model.Point;
import info.batyrev.research.optimization.core.model.ResultPair;
import info.batyrev.research.optimization.core.model.Simplex;
import info.batyrev.research.optimization.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NelderMeadMinimizer extends Minimizer {

    private Simplex simplex;

    public NelderMeadMinimizer(Calculable function, Simplex simplex) {
        super(function, Constants.Global.EPS);
        this.simplex = simplex;
    }

    public NelderMeadMinimizer(Calculable function, Simplex simplex, double precission) {
        super(function, precission);
        this.simplex = simplex;
    }

    @Override
    public ResultPair run() {
        List<Double> f = new ArrayList<>();

        int ndim = simplex.size() - 1;

        Point xh = new Point(); Point xg = new Point();
        Point xl = new Point(); Point xr = new Point();
        Point xe = new Point(); Point xs = new Point();
        double 	fh, fg, fl = Double.MAX_VALUE, fr, fe, fs;

        Point tempV = new Point();
        double tempD;

        for (int i = 0; i < simplex.size(); i++) {
            f.add(function.calculate(simplex.getVertex(i)));
        }

        int iter = 0;

        while (!terminationCondition()) {
            boolean flag;
            Point xc = new Point();

            if (iter > Constants.Global.ITER_MAX) {
                break;
            }

            iter++;

            simplex.sortByOrder(f);
            Collections.sort(f);

            xh = simplex.getVertex(ndim);       fh = f.get(ndim);
            xg = simplex.getVertex(ndim - 1); fg = f.get(ndim - 1);
            xl = simplex.getVertex(0);        fl = f.get(0);

            for (int i = 0; i < ndim; i++) {
                xc = xc.add(simplex.getVertex(i));
            }

            xc = xc.div((double) ndim);

            xr = xc.mult(1.0 + Constants.NelderMead.ALPHA).sub(xh.mult(Constants.NelderMead.ALPHA));
            fr = function.calculate(xr);

            if (fr <= fl) {
                xe = xc.mult(1.0 - Constants.NelderMead.GAMMA).add(xr.mult(Constants.NelderMead.GAMMA));
                fe = function.calculate(xe);

                if (fe < fl) {
                    simplex.setVertex(ndim, xe);
                    f.set(ndim, fe);
                } else {
                    simplex.setVertex(ndim, xr);
                    f.set(ndim, fr);
                }
            }

            if (fl < fr && fr <= fg) {
                simplex.setVertex(ndim, xr);
                f.set(ndim, fr);
            }

            flag = false;

            if (fh >= fr && fr > fg) {
                flag = true;

                tempD = fh;
                tempV = xh;

                simplex.setVertex(ndim, xr);
                f.set(ndim, fr);

                xr = tempV;
                fr = tempD;
            }

            if (fr > fh)
                flag = true;

            if (flag) {
                xs = xh.mult(Constants.NelderMead.BETA).add(xc.mult(1.0 - Constants.NelderMead.BETA));
                fs = function.calculate(xs);

                if (fs < fh) {
                    tempD = fh;
                    tempV = xh;

                    simplex.setVertex(ndim, xs);
                    f.set(ndim, fs);

                    xs = tempV;
                    fs = tempD;
                } else {
                    for (int i = 0; i < simplex.size(); i++) {
                        Point vertex = xl.add(simplex.getVertex(i).sub(xl).div(2.0));
                        simplex.setVertex(i, vertex);
                    }
                }
            }
        }

        ResultPair resultPair = new ResultPair(xl, fl);
        return resultPair;
    }

    @Override
    protected boolean terminationCondition() {
        Point center = new Point();

        for (int i = 0; i < simplex.size(); i++) {
            center = center.add(simplex.getVertex(i));
        }

        center = center.div((double) simplex.size());

        for (int i = 0; i < simplex.size(); i++) {
            double distance = center.calcDistance(simplex.getVertex(i));
            if (distance > precission) {
                return false;
            }
        }

        return true;
    }
}
