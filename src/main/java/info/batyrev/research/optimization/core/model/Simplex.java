package info.batyrev.research.optimization.core.model;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Simplex implements Printable {

    private static int POINTS_COUNT = Space.getDimension() + 1;

    private List<Point> vertices;

    public Simplex() {
        vertices = new ArrayList<>();

        for (int i = 0; i < POINTS_COUNT; i++) {
            vertices.add(new Point());
        }
    }

    public Simplex(List<Point> pointList) {
        if (pointList.size() != POINTS_COUNT) {
            throw new IllegalArgumentException("Incorrect number of points");
        }

        vertices = new ArrayList<>();
        vertices.addAll(pointList);
    }

    public Point getVertex(int i) {
        return vertices.get(i);
    }

    public void setVertex(int i, Point p) {
        vertices.set(i, p);
    }

    public int size() {
        return vertices.size();
    }

    public void sortByOrder(List<Double> ordinalValues) {
        if (ordinalValues.size() != vertices.size()) {
            throw new IllegalArgumentException("Invalid ordinal array");
        }

        for (int i = 0; i < vertices.size() - 1; i++) {
            if (ordinalValues.get(i) > ordinalValues.get(i + 1)) {
                double tempValue = ordinalValues.get(i + 1);
                ordinalValues.set(i + 1, ordinalValues.get(i));

                Point tempVertex = vertices.get(i + 1);
                vertices.set(i + 1, vertices.get(i));
                
                int j = i;
                while (j > 0 && tempValue < ordinalValues.get(j - 1)) {
                    ordinalValues.set(j, ordinalValues.get(j - 1));
                    vertices.set(j, vertices.get(j - 1));
                    j--;
                }

                ordinalValues.set(j, tempValue);
                vertices.set(j, tempVertex);
            }
        }
    }

    @Override
    public void print(Writer writer) {
        try {
            for (Point vertex : vertices) {
                vertex.print(writer);
            }
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Simplex{" + vertices.stream().map(Point::toString).collect(Collectors.joining(", ")) + "}";
    }
}
