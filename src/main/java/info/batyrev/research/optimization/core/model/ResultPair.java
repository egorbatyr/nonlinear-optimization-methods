package info.batyrev.research.optimization.core.model;

import java.text.MessageFormat;

public class ResultPair {

    private Point point;
    private double functionValue;

    private ResultPair() {}

    public ResultPair(Point point, double functionValue) {
        this.point = point;
        this.functionValue = functionValue;
    }

    public Point getPoint() {
        return point;
    }

    public double getFunctionValue() {
        return functionValue;
    }

    @Override
    public String toString() {
        return MessageFormat.format("ResultPair'{'functionValue={0} at {1}'}'", functionValue, point.toString());
    }
}
