package info.batyrev.research.optimization.core.model;

public final class Space {

    private static int dimension;

    private Space() {
    }

    public static void setDimension(int dimension) {
        if (dimension < 2) {
            throw new IllegalArgumentException("Invalid dimension of the space");
        }

        Space.dimension = dimension;
    }

    public static int getDimension() {
        if (dimension < 2) {
            throw new IllegalStateException("Invalid dimension of the space");
        }

        return dimension;
    }
}
