package info.batyrev.research.optimization.core.model;

import java.io.Writer;

public interface Printable {

    void print(Writer writer);
}
