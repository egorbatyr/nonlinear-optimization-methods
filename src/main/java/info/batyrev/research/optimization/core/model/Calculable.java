package info.batyrev.research.optimization.core.model;

public interface Calculable {

    double calculate(Point point);
}
