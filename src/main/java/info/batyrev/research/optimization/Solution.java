package info.batyrev.research.optimization;

import info.batyrev.research.optimization.calculation.FirstOptimizationFunction;
import info.batyrev.research.optimization.calculation.Randomizer;
import info.batyrev.research.optimization.calculation.SecondOptimizationFunction;
import info.batyrev.research.optimization.core.Minimizer;
import info.batyrev.research.optimization.core.NelderMeadMinimizer;
import info.batyrev.research.optimization.core.model.*;
import info.batyrev.research.optimization.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    private static void secondOptimization() {
        for (int runId = 0; runId < Constants.Global.MAXN; runId++) {
            List<Simplex> simplexList = new ArrayList<>();

            for (int i = 0; i < Constants.Global.NUM_AREAS; i++) {
                Simplex simplex = new Simplex();

                for (int j = 0; j < simplex.size(); j++) {
                    Point point = Randomizer.getPoint();
                    simplex.setVertex(j, point);
                }

                simplexList.add(simplex);
            }

            double length = Constants.SecondOptimization.L;
            double psy = Constants.SecondOptimization.psy_S;
            double m = Constants.SecondOptimization.m;
            double n = Constants.SecondOptimization.n;

            List<Double> time = new ArrayList<>();
            List<Double> gtime = new ArrayList<>();
            double localTime = 0, globalTime = 0.0;

            for (int i = 0; i < Constants.InputData.timeSet2.size(); i++) {
                if (i == 9 || i == 17 || i == 29 || i == 48
                        || i == 54) {
                    localTime = 0.0;
                } else {
                    localTime += Constants.InputData.timeSet2.get(i);
                }

                globalTime += Constants.InputData.timeSet2.get(i);
                gtime.add(globalTime);

                time.add(localTime);
            }

            Calculable function = new SecondOptimizationFunction(
                    length, time, psy, m, n, Constants.InputData.tetaqSet2
            );

            final double[] ans = {Double.MAX_VALUE};
            final Point[] point = {new Point()};

            simplexList.stream().forEach(simplex -> {
                        Minimizer minimizer = new NelderMeadMinimizer(function, simplex);
                        ResultPair resultPair = minimizer.run();

                        if (resultPair.getFunctionValue() + Constants.Global.EPS < ans[0]) {
                            ans[0] = resultPair.getFunctionValue();
                            point[0] = resultPair.getPoint();
                        }

                        System.out.println("pre ans = " + ans[0] + " point = " + point[0].toString());
                    }
            );

            System.out.printf("%.5f ", ans[0]);
            System.out.println(point[0].toString());
        }
    }

    public static void firstOptimization() {
        for (int runId = 0; runId < 100; runId++) {
            List<Simplex> simplexList = new ArrayList<>();

            for (int i = 0; i < 100; i++) {
                Simplex simplex = new Simplex();

                for (int j = 0; j < simplex.size(); j++) {
                    Point point = Randomizer.getPoint();
                    simplex.setVertex(j, point);
                }

                simplexList.add(simplex);
            }

            List<Double> psy = Constants.FirstOptimization.psy;
            List<Double> teta = Constants.FirstOptimization.teta;
//            List<Double> psy = Constants.FirstOptimizationInterpolated.psy;
//            List<Double> teta = Constants.FirstOptimizationInterpolated.teta;

//            for (int i = 0; i < psy.size(); i++) {
//                System.out.println("psy = " + psy.get(i) + " teta = " + teta.get(i));
//            }

            Calculable function = new FirstOptimizationFunction(psy, teta);

            final double[] ans = {Double.MAX_VALUE};
            final Point[] point = {new Point()};

            simplexList.parallelStream().forEach(simplex -> {
                        Minimizer minimizer = new NelderMeadMinimizer(function, simplex);
                        ResultPair resultPair = minimizer.run();

                        if (resultPair.getFunctionValue() + Constants.Global.EPS < ans[0]) {
                            ans[0] = resultPair.getFunctionValue();
                            point[0] = resultPair.getPoint();
                        }
                    }
            );

            System.out.printf("%.10f ", ans[0]);
            System.out.println(point[0].toString());
        }
    }

    public static void proections() {
        Point point = Randomizer.getPoint();

        double length = Constants.SecondOptimization.L;
        double psy = Constants.SecondOptimization.psy_S;
        double m = Constants.SecondOptimization.m;
        double n = Constants.SecondOptimization.n;
        List<Double> time = new ArrayList<>();
        List<Double> gtime = new ArrayList<>();
        double localTime = 0, globalTime = 0.0;

        Calculable function = new SecondOptimizationFunction(
                length, time, psy, m, n, Constants.InputData.tetaqSet2
        );

        double value = function.calculate(point);
    }

    public static void main(String[] args) {
        Space.setDimension(Constants.Global.CNT_DIMENSIONS);

        long stime = System.currentTimeMillis();

//        secondOptimization();

        firstOptimization();

        long ftime = System.currentTimeMillis();
        System.out.println("time = " + (double) (ftime - stime) / 1000.0);
    }
}
